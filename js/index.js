//Usage


//load your JSON (you could jQuery if you prefer)
function loadJSON(callback) {
// let controlData = JSON.stringify(...data)
  // console.log(controlData);
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', './wheel_data.json', true); 
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      //Call the anonymous function (callback) passing in the response
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
  // callback(controlData);
}

function loadDemoJSON(callback) {
// let controlData = JSON.stringify(...data)
  // console.log(controlData);
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', './demo.json', true); 
  xobj.onreadystatechange = function() {
    if (xobj.readyState == 4 && xobj.status == "200") {
      //Call the anonymous function (callback) passing in the response
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
  // callback(controlData);
}

//your own function to capture the spin results
function myResult(e) {
  //e is the result object
    console.log('Spin Count: ' + e.spinCount + ' - ' + 'Win: ' + e.win + ' - ' + 'Message: ' +  e.msg);
    // document.getElementById("ppw").innerHTML = "KSH 0";
    checkBetPicked()
    disableBtns()
    // if you have defined a userData object...

    if(e.userData){
      
      console.log('User defined score: ' + e.userData.score)

    }

}

//your own function to capture any errors
function myError(e) {
  //e is error object
  console.log('Spin Count: ' + e.spinCount + ' - ' + 'Message: ' +  e.msg);

}

function myGameEnd(e) {
  
  //e is gameResultsArray
  console.log(e);
  // TweenMax.delayedCall(5, function(){
    
  //   Spin2WinWheel.reset();

  // })


}

function loggedIn() {
        if (!sessionStorage.getItem("username")) {
        return false
      }else{
        return true
      }
}

function init() {
      //check if logged in 
    if (loggedIn()) {
      console.log('this is not a demo')
      $('.demo').removeClass('show')
      
            loadJSON(function(response) {
        // Parse JSON string to an object
        var jsonData = JSON.parse(response);
        // console.log(jsonData);
        //if you want to spin it using your own button, then create a reference and pass it in as spinTrigger
        var mySpinBtn = document.querySelector('.spinBtn');

        //create a new instance of Spin2Win Wheel and pass in the vars object
        var myWheel = new Spin2WinWheel();
        
        //WITH your own button
        myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError, spinTrigger:mySpinBtn});
        
        //WITHOUT your own button
        //myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError});
      });
      
 
    }else{
      console.log('this is a demo')

      $('.demo').addClass('show')

      loadDemoJSON(function (response) {
               // Parse JSON string to an object
        var jsonData = JSON.parse(response);
        // console.log(jsonData);
        //if you want to spin it using your own button, then create a reference and pass it in as spinTrigger
        var mySpinBtn = document.querySelector('.spinBtn');

        //create a new instance of Spin2Win Wheel and pass in the vars object
        var myWheel = new Spin2WinWheel();
        
        //WITH your own button
        myWheel.init({data:jsonData, onResult:myResult, onGameEnd:myGameEnd, onError:myError, spinTrigger:mySpinBtn});
        
      })

    }
}



//And finally call it
init();
